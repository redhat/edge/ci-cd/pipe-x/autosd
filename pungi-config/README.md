# pungi-config

A basic set of pungi configurations to build the AutoSD compose(s).

## autosd.conf

This pungi configuration builds the AutoSD compose.

## Composing Locally

You can generate a compose on your local machine using Pungi. Since the AutoSD packages are comming from different koji instances, it is need to required to run pungi multiple times. Therefore, different repos can be merged into a compose.

Auto-Toolchain maintains a Pungi container [1] that can be used for this purpose.

```
podman run \
  -v $(pwd):/mnt \
  -v $(pwd)/cbs-cache:/root/cbs-cache \
  -v $(pwd)/cbs-cache:/root/cs-cache \
  -it quay.io/automotive-toolchain/pungi:latest \
  /bin/bash -c "cd /mnt; bash"
```

This will drop you into a prompt where you can execute pungi-koji.

```
$ cp pungi-config/koji.conf.d/* /etc/koji.conf.d/
$ pungi-koji --compose-dir ./autosd-kernel --config ./pungi-config/autosd-kernel.conf --test
$ pungi-koji --compose-dir ./autosd-packages --config ./pungi-config/autosd-packages.conf --test
$ pungi-koji --compose-dir ./autosd --config ./pungi-config/autosd.conf --test
```

## Appendix

[1] https://gitlab.com/redhat/edge/ci-cd/pipe-x/tools/auto-toolchain-containers/-/tree/main/pungi
